from django.urls import path
from lesson3 import views

urlpatterns = [
    path('main/', views.index, name='index-view'),
    path('text/', views.text, name="text"),
    path('file/', views.my_file, name="file"),
    path('json/', views.json, name="json"),
    path('starwars/', views.star_wars, name='star-wars'),
    path('luke/', views.luke, name='luke'),
    path('leia/', views.leia, name='leia'),
    path('han/', views.han, name='han'),
    path('filters/', views.filters, name="filters"),
    path('tags-for/', views.tags_for, name="tags-for"),
    path('tags-for-empty/', views.tags_for_empty, name="tags-for-empty"),

    path('class-view/<type>/', views.MyView.as_view(), name='class_view')
]


