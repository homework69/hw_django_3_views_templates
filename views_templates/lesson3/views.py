from django.shortcuts import render
from django.http import HttpRequest, HttpResponse, JsonResponse, FileResponse, HttpResponseNotAllowed
from django.templatetags.static import static
from django.views import View


def index(request: HttpRequest) -> HttpResponse:
    return render(request, 'main.html')


def text(request):
    return HttpResponse("This is text from backend to user interface")


def file(request):
    # with open() as file:
    #     work with file
    print(static('lesson3/img/001.jpg'))
    response = FileResponse(open(static('lesson3/img/001.jpg'), "rb+"))
    print(response.status_code)
    return response


def my_file(request):
    filename = "my-file.txt"
    content = 'Вот ваш файл'
    response = HttpResponse(
        content,
        headers={
            'content_type': 'text/plain',
            'Content-Disposition': 'attachment; filename={0}'.format(filename)},
    )
    # response.status_code = 227
    print(response.status_code)
    return response


def json(request):
    list_out = [
        {'priority': 100, 'task': 'Составить список дел'},
        {'priority': 150, 'task': 'Изучать Django'},
        {'priority': 1, 'task': 'Подумать о смысле жизни'},
    ]
    json_dumps_params = {
        'ensure_ascii': False,
        'indent': 4,
    }
    return JsonResponse(list_out, safe=False, json_dumps_params=json_dumps_params)


def filters(request):
    list_out = [
        {'priority': 100, 'task': 'Составить список дел'},
        {'priority': 150, 'task': 'Изучать Django'},
        {'priority': 1, 'task': 'Подумать о смысле жизни'},
    ]
    context = {
        'list': list_out,
    }
    return render(request, 'filters.html', context)


def tags_for(request):
    list_out = [
        {'name': 'Шаддам IV', 'surname': 'Коррино'},
        {'name': 'Пол', 'surname': 'Атрейдес'},
        {'name': 'Франклин', 'surname': 'Герберт'},
    ]
    context = {
        'list': list_out,
    }
    return render(request, 'tags_for.html', context)


def tags_for_empty(request):
    list_out = [
        {'id': 1, 'question_text': 'В чем смысл жизни?'},
        {'id': 2, 'question_text': 'Что первично, дух или материя?'},
        {'id': 3, 'question_text': 'Существует ли свобода воли?'},
    ]
    context = {
        'list': list_out,
    }
    return render(request, 'tags_for_empty.html', context)

# Вселенная Звездных воен


def star_wars(request):
    return render(request, 'movies.html')


def luke(request):
    context = {
        'name': 'Люк Скайуокер',
        'description': 'Люк Скайуокер — один из главных персонажей вселенной «Звёздных войн», джедай, '
                       'сын сенатора с Набу Падме Амидалы Наберри и рыцаря-джедая Энакина Скайуокера.',
    }
    return render(request, 'personage.html', context)


def leia(request):
    context = {
        'name': 'Лея Органа',
        'description': 'Лея Органа — дочь рыцаря-джедая Энакина Скайуокера и сенатора Падме Амидалы Наберри.',
    }
    return render(request, 'personage.html', context)


def han(request):
    context = {
        'name': 'Хан Соло',
        'description': 'Хан Соло — пилот космического корабля «Тысячелетний сокол», его бортмехаником '
                       'и вторым пилотом является вуки по имени Чубакка.',
    }
    return render(request, 'personage.html', context)


# Class Based View


class MyView(View):

    def get(self, request, type):
        print(request)
        print(type)
        print(request.GET)

        if type == "file":
            filename = "my-file.txt"
            content = 'Вот ваш файл'
            response = HttpResponse(
                content,
                headers={
                    'content_type': 'text/plain',
                    'Content-Disposition': 'attachment; filename={0}'.format(filename)},
            )
            return response
        elif type == "json":
            list_out = [
                {'priority': 100, 'task': 'Составить список дел'},
                {'priority': 150, 'task': 'Изучать Django'},
                {'priority': 1, 'task': 'Подумать о смысле жизни'},
            ]
            json_dumps_params = {
                'ensure_ascii': False,
                'indent': 4,
            }
            return JsonResponse(list_out, safe=False, json_dumps_params=json_dumps_params)
        elif type == "text":
            print('Prepared text')
            return HttpResponse('This is text from Class Based View!')
        elif type == "html":
            return HttpResponse('<h1> This is html from Class Based View! </h1>')
        else:
            return HttpResponseNotAllowed("You shall not pass!!!")
